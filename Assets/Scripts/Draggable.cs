﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    public int upDir = 1;

    private Vector2 dragbase = Vector2.zero;
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragbase = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 relative = ((Vector2)Input.mousePosition - dragbase) * 0.01f;

            transform.position += Vector3.right * relative.y * upDir;
            //transform.position += Vector3.forward * -relative.x;

            dragbase = Input.mousePosition;
        }


        if (Input.GetMouseButtonUp(0))
        {
            dragbase = Vector2.zero;
        }
    }
}
