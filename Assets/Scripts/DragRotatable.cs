﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragRotatable : MonoBehaviour
{
    public Text debugText;

    public GameObject h;
    public GameObject v;

    public GameObject slicerRoot;
    public GameObject sliceableRoot;
    public Tween slicerTween;
    public Animator slicerAnim;
    float lengthX = 0;
    float lengthY = 0;
    //float currentDir = 0;
    Vector3 tweenStartPos = Vector3.zero;
    Vector3 tweenEndPos = Vector3.zero;
    private bool sliced = false;
    private bool sliceStarted = false;
    //private bool flipped = false;
    public SlicerVisual slicerVisual;

    public int upDir = 1;
    public float sensitivity;
    private Vector2 dragbase = Vector2.zero;
    private Vector2 lastMousePos = Vector2.zero;
    private Vector2 initMousePos = Vector2.zero;
    private float angleOffset = 0;

    public float skinParticleGapStep = 5;
    private float skinParticleGap = 0;

    Transform intendedTransformH;
    Transform intendedTransformV;

    //bool moveleft = false;
    //bool moveright = false;
    public bool moving = false;

    private GameManager gm;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();

        intendedTransformH = new GameObject().transform;
        intendedTransformV = new GameObject().transform;
        intendedTransformV.SetParent(intendedTransformH);
        h.transform.position = Vector3.forward * slider.value * 5 - Vector3.forward * 2f;
    }
    public void MoveLeft()
    {
        sliceableRoot.transform.position += Vector3.forward * 0.2f;
        initMousePos = Input.mousePosition;
        sliced = false;
        lengthX = 0;
        lengthY = 0;
    }
    public void StopMoveLeft()
    {

    }

    public void MoveRight()
    {
        sliceableRoot.transform.position += Vector3.back * 0.2f;
        initMousePos = Input.mousePosition;
        sliced = false;
        lengthX = 0;
        lengthY = 0;
    }

    public void StopMoveRight()
    {

    }

    public Slider slider;
    public void UpdatePosition()
    {
        h.transform.position = Vector3.forward * slider.value * 5 - Vector3.forward * 2f;
        initMousePos = Input.mousePosition;
        sliced = false;
        lengthX = 0;
        lengthY = 0;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragbase = Input.mousePosition;
            lastMousePos = Input.mousePosition;
            if (!sliceStarted)
            {
                sliceStarted = true;
                initMousePos = Input.mousePosition;
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (moving || gm.levelComplete)
            {
                return;
            }

            float relativeX = -(Input.mousePosition.x - lastMousePos.x) * sensitivity * 1f;
            float relativeY = (Input.mousePosition.y - lastMousePos.y) * sensitivity * 1f;
            //length = (Input.mousePosition.y - initMousePos.y) * sensitivity;


            //if (Mathf.Sign(relative) != currentDir)
            //{
            //    flipped = true;
            //}

            //if (flipped)
            //{
            //    flipped = false;
            //    angleOffset = Mathf.Abs(length) * 1 - Mathf.Abs(angleOffset) * 1;
            //}

            //if (relative != 0)
            //{
            //    currentDir = Mathf.Sign(relative);
            //}

            lengthX += relativeY;
            lengthY += relativeX;

            //debugText.text = "l :" + length.ToString("F0") + "\nc: " + currentDir.ToString("F0") + "\na : " + angleOffset.ToString("F0");

            skinParticleGap += Mathf.Abs(relativeY + relativeX);

            if (skinParticleGap >= skinParticleGapStep)
            {
                skinParticleGap = 0;
                slicerVisual.SkinParticle();
            }

            intendedTransformH.Rotate(Vector3.right, -relativeX, Space.World);
            intendedTransformV.transform.Rotate(transform.forward, relativeY, Space.World);

            //transform.Rotate(Vector3.forward * relativeY * upDir);
            //transform.Rotate(Vector3.right * relativeX * upDir);
            //transform.position += Vector3.forward * -relative.x;

            if (Mathf.Abs(lengthX) >= 170 | Mathf.Abs(lengthX) <= 190)
            {
                tweenStartPos = slicerVisual.rayHit.point;
            }



            if (Mathf.Abs(lengthX) >= 340 - angleOffset && !sliced)
            {
                tweenEndPos = slicerVisual.rayHit.point;

                slicerRoot.transform.eulerAngles = Vector3.up * (Mathf.Atan2(-lengthX, -lengthY) * Mathf.Rad2Deg + 90);
                //Debug.Log(Mathf.Atan2(-lengthX, -lengthY) * Mathf.Rad2Deg + 90);

                lengthX = 0;
                sliced = true;
                slicerVisual.hideAllSkinParticles();
                slicerAnim.Play("SliceX", 0, 0);
                initMousePos = Input.mousePosition;
                MoveRight();
                MoveLeft();
                //slicerTween.enabled = true;
                //slicerTween.startPos = tweenStartPos;
                //slicerTween.endpos = tweenEndPos;
            }

            if (Mathf.Abs(lengthY) >= 340 - angleOffset && !sliced)
            {
                tweenEndPos = slicerVisual.rayHit.point;

                slicerRoot.transform.eulerAngles = Vector3.up * (Mathf.Atan2(-lengthX, -lengthY) * Mathf.Rad2Deg + 0);
                //Debug.Log(Mathf.Atan2(-lengthX, -lengthY) * Mathf.Rad2Deg + 0);

                lengthY = 0;
                sliced = true;
                slicerVisual.hideAllSkinParticles();
                slicerAnim.Play("SliceZ", 0, 0);
                initMousePos = Input.mousePosition;
                MoveRight();
                MoveLeft();
            }

            lastMousePos = Input.mousePosition;
        }


        if (Input.GetMouseButtonUp(0))
        {
            dragbase = Vector2.zero;
            lastMousePos = Vector2.zero;
        }


        h.transform.rotation = Quaternion.Slerp(h.transform.rotation, intendedTransformH.rotation, Time.deltaTime * 5f);
        v.transform.rotation = Quaternion.Slerp(v.transform.rotation, intendedTransformV.rotation, Time.deltaTime * 5f);

    }
}
