﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public List<ScrapItem> scrapItems = new List<ScrapItem>();
    public List<Salvage> salvage = new List<Salvage>();
}
