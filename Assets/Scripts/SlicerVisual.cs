﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlicerVisual : MonoBehaviour
{
    public ParticleSystem streaks;
    public GameObject slicerVisualRoot;
    public RaycastHit rayHit = new RaycastHit();
    public GameObject skinParticlesRoot;
    private List<GameObject> skinParticles = new List<GameObject>();
    private int currentSkinParticle = 0;

    bool hiding = false;

    private void Start()
    {
        for (int i = 0; i < skinParticlesRoot.transform.childCount; i++)
        {
            skinParticles.Add(skinParticlesRoot.transform.GetChild(i).gameObject);
        }
    }

    private void FixedUpdate()
    {

        if (Physics.Raycast(transform.position, transform.position + Vector3.down * 100, out rayHit))
        {
            if (rayHit.distance < 7f)
            {
                slicerVisualRoot.transform.position = Vector3.Lerp(slicerVisualRoot.transform.position, rayHit.point, Time.fixedDeltaTime * 12f);
            }
        }
    }

    public void hideAllSkinParticles()
    {
        foreach (var item in skinParticles)
        {
            item.SetActive(false);
        }
        hiding = true;
        Invoke("StopHiding", 1f);
    }

    void StopHiding()
    {
        hiding = false;
    }
    public void SkinParticle()
    {
        if (hiding)
        {
            return;
        }

        if (rayHit.distance > 7)
        {
            return;
        }
        streaks.transform.position = rayHit.point;
        streaks.Play();
        //Invoke("StopStreaks", 0.85f);
        //Debug.Log("skin particle");
        skinParticles[currentSkinParticle].SetActive(true);
        skinParticles[currentSkinParticle].transform.position = rayHit.point/* - transform.up * 0.15f*/;
        currentSkinParticle++;
        if (currentSkinParticle >= skinParticles.Count)
        {
            currentSkinParticle = 0;
        }
    }

    void StopStreaks()
    {
        streaks.Stop();
    }
}
