﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Slider slider;
    public GameObject inventory;

    public GameObject levelCompleteUI;
    public Text[] salvage;
    public int salvageCollected = 0;

    public bool levelComplete = false;

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    private void Start()
    {
        inventory.transform.localPosition = Vector3.zero;
    }

    public void Revealed(string text, int slot)
    {
        StartCoroutine(RevealDelayed(text, slot));

    }

    IEnumerator RevealDelayed(string text, int slot)
    {
        yield return new WaitForSeconds(1f);

        salvage[slot].text = text;
        salvage[slot].gameObject.SetActive(true);
        salvageCollected++;


        if (salvageCollected >= salvage.Length)
        {
            Invoke("LevelComplete", 1f);
        }
    }

    private void LevelComplete()
    {
        levelComplete = true;
        levelCompleteUI.SetActive(true);
        slider.gameObject.SetActive(false);
        inventory.transform.localPosition = Vector3.up * 182;
    }
}
