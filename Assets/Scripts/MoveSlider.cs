﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MoveSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public DragRotatable dragRotatable;

    public void OnPointerDown(PointerEventData eventData)
    {
        dragRotatable.moving = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        dragRotatable.moving = false;
    }
}
