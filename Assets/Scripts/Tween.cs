﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tween : MonoBehaviour
{
    public Vector3 startPos = Vector3.zero;
    public Vector3 endpos = Vector3.zero;
    public float speed = 5;

    private float interpolation = 0;

    //private void Awake()
    //{
    //    enabled = false;
    //}

    private void OnEnable()
    {
        Debug.Log("enabled!");
        interpolation = 0;
    }

    private void FixedUpdate()
    {
        if (interpolation < 1)
        {
            interpolation += Time.fixedDeltaTime * speed;
        }
        else
        {
            enabled = false;
        }

        transform.position = Vector3.Lerp(startPos, endpos, interpolation);
    }

}
