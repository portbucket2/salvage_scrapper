﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salvage : MonoBehaviour
{
    public string salvageName;
    public int inventorySlot;

    private GameManager gm;

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Sliceable"))
        {
            gm.Revealed(salvageName, inventorySlot);
            Invoke("Hide", 1f);
            GetComponent<BoxCollider>().enabled = false;
        }
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
